import org.gradle.api.tasks.Copy
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

group = "org.webscene"
version = "0.1-SNAPSHOT"

buildscript {
    var kotlinVer: String by extra
    kotlinVer = "1.2.60"

    repositories {
        jcenter()
        mavenCentral()
    }

    dependencies {
        classpath(kotlin(module = "gradle-plugin", version = kotlinVer))
    }
}

apply {
    plugin("kotlin2js")
}

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("${System.getProperty("user.home")}/.m2/repository") }
}

val kotlinVer: String by extra

dependencies {
    val websceneVer = "0.1"
    "compile"("org.jetbrains.kotlin:kotlin-stdlib-js:$kotlinVer")
    "compile"("org.webscene:webscene-core-js:$websceneVer")
    "compile"("org.webscene:webscene-bootstrap-js:$websceneVer")
}

val webDir = "${projectDir.absolutePath}/web"
val compileKotlin2Js by tasks.getting(Kotlin2JsCompile::class) {
    val fileName = "notifier.js"
    kotlinOptions {
        outputFile = "$webDir/js/$fileName"
        sourceMap = true
        moduleKind = "umd"
    }
    doFirst { File(webDir).deleteRecursively() }
}
val build by tasks
val assembleWeb by tasks.creating(Copy::class) {
    dependsOn("classes")
    configurations["compile"].forEach { file ->
        from(zipTree(file.absolutePath)) {
            includeEmptyDirs = false
            include { fileTreeElement ->
                val path = fileTreeElement.path
                path.endsWith(".js") && path.startsWith("META-INF/resources/") ||
                    !path.startsWith("META_INF/")
            }
        }
    }
    from(compileKotlin2Js.destinationDir)
    into("$webDir/js")
}

task<Copy>("deployClient") {
    dependsOn(compileKotlin2Js, assembleWeb)
    from("${projectDir.absolutePath}/assets/web")
    into(webDir)
}