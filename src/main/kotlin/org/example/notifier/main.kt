package org.example.notifier

import org.w3c.dom.Element
import org.w3c.dom.events.Event
import org.w3c.notifications.NotificationOptions
import org.webscene.bootstrap.layout.initBootstrapLayout
import org.webscene.core.dom.DomQuery
import org.webscene.core.dom.focus
import org.webscene.core.dom.textBoxValue
import org.webscene.core.notification

@Suppress("unused")
fun main(args: Array<String>) {
    initBootstrapLayout()
    setupWebPage()
    val msgTxt = DomQuery.elementById<Element>("msg-txt")
    val showNotificationBtn = DomQuery.elementById<Element>("show-notification-btn")
    showNotificationBtn.addEventListener(type = "click", callback = onShowNotificationBtnClick(msgTxt))
    msgTxt.focus()
}

private fun onShowNotificationBtnClick(msgTxt: Element?): (Event) -> Unit = {
    if (msgTxt.textBoxValue.isNotEmpty()) {
        notification(
            title = "Test Notification",
            options = msgNotificationOptions(msgTxt.textBoxValue)
        )
        msgTxt.textBoxValue("")
        msgTxt.focus()
    }
}

private fun msgNotificationOptions(msg: String) = NotificationOptions(body = msg)
