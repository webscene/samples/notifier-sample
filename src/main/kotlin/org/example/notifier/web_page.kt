package org.example.notifier

import org.webscene.bootstrap.layout.BootstrapLayout
import org.webscene.bootstrap.layout.Column
import org.webscene.bootstrap.layout.ColumnSize
import org.webscene.bootstrap.layout.container_ext.toDomElement
import org.webscene.core.dom.DomEditor
import org.webscene.core.html.element.input.InputType
import org.webscene.core.html.HtmlCreator as html
import org.webscene.core.html.HtmlInputCreator as htmlInput

private fun createColumns() = arrayOf(
    BootstrapLayout.column(ColumnSize.MEDIUM to 2) { id = "lbl-col" },
    BootstrapLayout.column(ColumnSize.MEDIUM to 4) { id = "txt-col" },
    BootstrapLayout.column(ColumnSize.MEDIUM to 6) { id = "btn-col" }
)

private fun populateColumns(columns: Array<Column>) {
    val lblColPos = 0
    val txtColPos = 1
    val btnColPos = 2

    columns[lblColPos].children += createMsgLbl()
    columns[txtColPos].children += createMsgTxt()
    columns[btnColPos].children += createShowNotificationBtn()
}

private fun createMsgLbl() = html.element("label") {
    id = "msg-lbl"
    +"Message: "
    attributes["for"] = "msg-txt"
    classes += "text-center"
}

private fun createMsgTxt() = htmlInput.input(type = InputType.TEXT) {
    id = "msg-txt"
    attributes["placeholder"] = "Enter message"
}

private fun createShowNotificationBtn() = htmlInput.button {
    id = "show-notification-btn"
    classes += "btn"
    classes += "btn-sm"
    classes += "btn-default"
    attributes["type"] = "submit"
    attributes["value"] = "Show Notification"
    +"Show Notification"
}

internal fun setupWebPage() {
    val columns = createColumns()
    val mainLayout = BootstrapLayout.container {
        fullWidth = false
        children += html.div {
            classes += "page-header"
            children += html.heading { +"Notifier" }
        }
    }

    populateColumns(columns)
    mainLayout.row {
        id = "row"
        classes += "vertical-align"
        columns.forEach { children += it }
    }
    DomEditor.editSection(domElement = mainLayout.toDomElement())
}
